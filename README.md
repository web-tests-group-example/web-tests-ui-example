# UI tests implementation on CI

The example of a pipeline design for UI test execution on Gitlab CI

## Pipeline structure

The pipeline diagram:

[![](resources/pipeline-diagram.png)](https://gitlab.com/web-tests-group-example/web-tests-ui-example/-/pipelines/latest)


The pipeline consists of two consequent stages "Test" and "Deploy":
* At the first "Test" stage, the `tests` job is executing. It runs tests and saves the XML results as artifacts.
  The console text report is also available in the job [log](https://gitlab.com/web-tests-group-example/web-tests-ui-example/-/pipelines/557478157).
* At the next "Deploy" stage, the `pages` job is executing. It generates [Allure](https://github.com/allure-framework) report from the results and puts it in the `public` directory. [Log](https://gitlab.com/web-tests-group-example/web-tests-ui-example/-/jobs/2555969839) is also available.
  Finally, the system job `pages:deploy` deploys the report on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) server.

The pipeline starts by any push/merge activity. It is also possible to start the pipeline manually at any moment using the "Run pipeline" button in the [pipelines section](https://gitlab.com/web-tests-group-example/web-tests-ui-example/-/pipelines).

With this configuration, the contents of Gitlab Pages cleared before each deployment and therefore the history of runs not saved. Thus, only the latest run report is available.

The latest run report can be viewed by clicking on the badge:

[![](https://img.shields.io/badge/Gitlab_Pages-View_Report-blue)](https://web-tests-group-example.gitlab.io/web-tests-ui-example)

## Technology
* test framework - ```WebdriverIO```
* test runner - ```Mocha```
* reporting - ```Allure```

## Web application for tests
http://the-internet.herokuapp.com/
